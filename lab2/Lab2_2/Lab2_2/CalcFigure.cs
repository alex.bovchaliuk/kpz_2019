﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2_2
{
    class CalcFigure
    {
        public void GetLength(double r, ref double l)
        {
            l = (double)(2 * 3.1415 * r);
        }
        public void GetArea(double r, ref double s)
        {
            s = (double)(3.1415 * r * r);
        }
        public void GetVolume(double r, ref double v)
        {
            v = (double)(4.0 / 3.0 * 3.1415 * r * r * r);
        }
    }
}
