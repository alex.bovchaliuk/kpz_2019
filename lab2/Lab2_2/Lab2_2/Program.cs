﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2_2
{
    class Program
    {
        internal delegate void CalcFig(double r, ref double l);

        public static event CalcFig EvCF;

        static void Main(string[] args)
        {
            CalcFigure CF = new CalcFigure();
            double len, r;
            r = 1.0;
            len = 0;
            EvCF = CF.GetLength;
            EvCF(r, ref len);
            EvCF -= CF.GetLength;
            EvCF += CF.GetArea;
            EvCF(r, ref len);
            Console.WriteLine("Area");
            Console.WriteLine(len.ToString());
            EvCF -= CF.GetArea;
            EvCF += CF.GetVolume;
            EvCF(r, ref len);
            Console.WriteLine("Volume");
            Console.WriteLine(len.ToString());
            Console.ReadKey();
        }
    }
}
